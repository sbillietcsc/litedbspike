using System;
using System.Collections.Generic;

public static class EventGenerator {
  public static IEnumerable<CreatedScheduleEvent> Generate (int numberOfEvents) {
    for (int i = 0; i < numberOfEvents; i++) {
      var id = Guid.NewGuid();
      yield return new CreatedScheduleEvent () {
        Id = id,
        Name = $"Schedule {id}"
      };
    }
  }
}
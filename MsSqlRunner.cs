using System;
using System.Data.SqlClient;

public static class MsSqlRunner {
  public static void InsertEvents (int numberOfEvents) {
    var builder = new SqlConnectionStringBuilder ();
    builder.DataSource = "localhost,1533"; // update me
    builder.UserID = "sa"; // update me
    builder.Password = "4kCAWYesf86Mv3GkVXqF"; // update me
    builder.InitialCatalog = "TestDataDB";

    var counter = 0;
    foreach (var @event in EventGenerator.Generate (numberOfEvents)) {
      using (SqlConnection connection = new SqlConnection (builder.ConnectionString)) {
        connection.Open ();

        var sql = $"INSERT INTO Events (Id, Name) VALUES ('{@event.Id}', '{@event.Name}')";

        using (SqlCommand command = new SqlCommand (sql, connection)) {
          command.ExecuteNonQuery ();
          Console.WriteLine ($"Inserted event {counter}");
          counter++;
        }
      }
    }
  }
}

// public static void BulkInsertEvents (int numberOfEvents) {

// }
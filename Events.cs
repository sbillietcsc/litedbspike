using System;

public class CreatedScheduleEvent {
  public Guid Id { get; set; }
  public string Name { get; set; }
}
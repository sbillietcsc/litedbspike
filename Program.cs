﻿using System;
using System.Diagnostics;

namespace ConsoleApplication {
  public class Program {
    public static void Main (string[] args) {
      var stopWatch = new Stopwatch();
      stopWatch.Start();

      LiteDbRunner.InsertEvents(100000);

      stopWatch.Stop();
      var timeSpan = stopWatch.Elapsed;
      var elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds,
            timeSpan.Milliseconds / 10);
      Console.WriteLine("RunTime " + elapsedTime);
    }
  }
}
using System;
using LiteDB;

public static class LiteDbRunner {
  public static void InsertEvents (int numberOfEvents) {
    using (var db = new LiteDatabase (@"./data.db")) {
      var counter = 0;
      foreach (var @event in EventGenerator.Generate (numberOfEvents)) {
        var col = db.GetCollection<CreatedScheduleEvent> ("events");
        col.Insert (@event);
        Console.WriteLine ($"Inserted event {counter}");
        counter++;
      }
    }
  }

  public static void BulkInsertEvents (int numberOfEvents) {
    using (var db = new LiteDatabase (@"./data.db")) {
      var col = db.GetCollection<CreatedScheduleEvent>("events");   
      col.Insert(EventGenerator.Generate(numberOfEvents));
    }
  }
}